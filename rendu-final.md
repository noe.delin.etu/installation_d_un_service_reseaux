# Création de l'environnement virtuel

$${\textcolor{red}{Ceci\ est\ une\ présentation\ de\ la\ Sae\ 203}}$$

## Table des matières

- [Introduction](#introduction)
- [Rappels](#rappels)
- [Préparation](#pr-paration)

Groupe :

+ <thomas.gysemans.etu@univ-lille.fr>
+ <noe.delin.etu@univ-lille.fr>

## Introduction

Nous avons créé une machine virtuelle de type Linux, basée sur Debian (64-Bit), en utilisant `Oracle VM VirtualBox`, dont voici les configurations précises :

* Ram : 2048 Mb
* Disque dur : 20 Go entier
* Configuration réseau : `NAT`

![Config réseau](./pictures/config-reseau.png)

## Rappels

- Que signifie 64-Bit ?

Un système d'exploitation 64 Bit signifie qu'il fonctionne sur un PC dont le processeur peut gérer des morceaux de 64 bits de données à la fois.

- Quel est le nom du fichier XML contenant la configuration de la machine ?

Il s'agit de `sae203.vbox`. On peut modifier ce fichier pour faire croire à la VM que le PC utilise 2 processeurs. Dans le fichier XML, la ligne 23 indique `<CPU count="1">`. Il suffit de changer le "1" en "2".

- Qu'est-ce fichier ISO bootable ?

Un fichier ISO standard est un format de fichier numérique reproduisant un CD ou un DVD. Un fichier ISO bootable va contenir le code d'exécution d'un système d'exploitation que le PC va lire en priorité.

- Qu'est-ce que MATE ?

***MATE Desktop Environment*** est un paquet qui designe l'affichage graphique global du système d'exploitation. Il s'agit d'une alternative à __GNOME__.

- Qu'est-ce qu'un serveur web ?

Nous avons installé sur la machine un serveur Web qui permettra d'interpréter des requêtes HTTP envoyées par un client. L'ordinateur simulera donc un serveur web distant en localhost avec la technologie APACHE.

- Qu'est-ce qu'un serveur SSH ?

**SSH** signifie **Secure Shell**. C'est utilisé pour se connecter à un ordinateur distant dans le but d'accéder à des fichiers particuliers ou d'exécuter des tâches qui requièrent des droits d'administrateur.

- Qu'est-ce qu'un serveur mandataire ?

Le serveur mandataire (ou proxy) est un intermédiaire entre la machine et le réseau. Ceci permet de contrôler les paquets qui sont envoyés. Sur un réseau particulier on peut l'utiliser pour bloquer certaines URL par exemple.

- Quelle est la version du noyau Linux utilisé par notre VM ?

```bash
uname -r
# 5.10.0-21-amd64
```

- À quoi servent les suppléments invités ?

Ils permettent :

* Un meilleur confort en adaptant l'écran aux besoins de l'utilisateur
* Un `drag and drop` entre la machine hôte et la machine virtuelle voire même de partager le presse-papier.

- À quoi sert la commande `mount` ?

Par défaut le système de fichiers tout entier est basé sur le chemin nommé `/`. On peut changer ça avec la commande `mount` ou faire en sorte d'attacher un autre système de fichiers à un endroit particulier de l'arbre hiérarchique. C'est utile si nous avons des disques durs externes ou des partitions.

## Préparation du document de rendu

- Compilation en HTML :

Pour la compilation en _HTML_ nous avons utilisé [pandoc](https://pandoc.org/) :

```bash
pandoc --toc --standalone --mathjax -s Rendu.md -o Rendu.html --metadata pagetitle="rendu-saé203"
```

- Compilation en PDF :

Nous avons utilisé `pandoc` également mais il n'était pas possible sur Mac d'utiliser le générateur PDF par défaut donc nous avons également installé une alternative qui s'appelle `tectonic`. La commande est exactement la même, hormis le nom du fichier de destination (`Rendu.pdf`), l'option `--pdf-engine tectonic` et le fait que l'option `--mathjax` n'est pas nécessaire pour cette conversion.

- Don des droits sudo

```bash
sudo usermod -aG sudo user
```

> Il s'agit d'une commande que nous avions déjà vue auparavant.

On vérifie que l'utilisateur `user` appartienne bel et bien au groupe `sudo` :  

```bash
groups user
# ce qui retourne la liste des groupes auxquels user appartient
# on remarque que "sudo" est bien dans la liste
```

# Installation Debian automatisée par préconfiguration

_Site officiel de [Debian](https://www.debian.org)._

Pour l'installation automatisée, nous avons récupéré les fichiers nécessaires et avons procédé à la modification du fichier `preseed.cgf`.

Avant tout nous avons dû configurer le miroir :

![miroir](./pictures/mirror.png)

Dans un premier temps, nous avons ajouté `sudo` à l'utilisateur standard :

![ajout de sudo](./pictures/sudo-to-default-groups.png){ width=250px }

Ensuite, nous avons défini `mate-desktop` comme l'interface graphique par défaut de l'environnement :

![mate](./pictures/mate.png)

Enfin, nous avons installé les paquets suivants :

* sudo (sinon la gestion sudo est inutile)
* git, sqlite3, curl (pour un futur tp)
* bash-completion (pour simplifier l'écriture des commandes)
* neofetch (un outil personnalisable pour afficher les informations du système)

![ajout des paquets](./pictures/ajout-des-paquets.png)

Voilà, l'installation est désormais personnalisée.

## Questions - réponses

- D'où vient le nom de [Debian]{.mark} ?

Le nom vient de son créateur, Ian Murdock, and sa femme, Debra. Le projet est désormais réalisé par des développeurs indépendants en [open-source](https://github.com/Debian).

- Durées des versions de support

|Type de version|durée approximative|source|
|---------------|-------------------|------|
|LTS|au moins 5 ans|[LTS](https://wiki.debian.org/LTS)|
|ELTS|10 ans|[Extended](https://wiki.debian.org/LTS/Extended)|

En règle générale une version est destinée à être prise en charge pour une durée de 3 ans.

- Pendant combien de temps les mises à jour de sécurité seront-elles fournies ?

Environ 1 an si on regarde les [versions précédentes](https://wiki.debian.org/fr/DebianReleases) sorties.

- Quelles sont les versions de Debian activement maintenues ?

Actuellement les développeurs travaillent sur 4 types de version :

|Type de version|nom de code|numéro de version|
|---------------|-----------|-----------------|
|oldstable|Buster|10|
|stable|Bullseye|11|
|testing|Bookworm|12|
|unstable|Sid|-|

Sans compter [experimental](https://wiki.debian.org/fr/DebianExperimental) et [backport](https://wiki.debian.org/fr/Backports).

- D'où viennent les différents noms de code des versions de Debian ?

Toy Story de Pixar/Disney (1995) et c'est encore une fois la même [source](https://wiki.debian.org/fr/DebianReleases).

- Les architectures prises en charge par Bullseye :

|Architecture|Etiquette Debian|
|------------|----------------|
|AMD64 & Intel 64|amd64|
|Intel x86-based|i386|
|ARM|armel|
|ARM avec matériel FPU|armhf|
|ARM 64 bits|arm64|
|MIPS 32 bits (petit-boutien)|mips64el|
|MIPS 32 bits (petit-boutien)|mipsel|
|Power Systems|ppc64el|
|IBM S/390 64 bits|s390x|

[Source](https://www.debian.org/releases/stable/armel/ch02s01.fr.html)

- La première version (stable) :
  * Premier nom de code utilisé : [Buzz]{.underline}
  * annoncé le : 11 decembre 1995
  * Numéro de version : 1.1

[Source](https://www.debian.org/doc/manuals/project-history/releases.en.html) - [Source annonce de Debian 1.1](https://lists.debian.org/debian-announce/1995/msg00010.html)

- Dernière version
  * Premier nom de code utilisé : forky
  * annoncé le : 13 octobre 2022
  * Numéro de version : 14

[Source](https://wiki.debian.org/DebianForky)

# Gitea

Suite aux précédents tp nous avons d'ores et déjà sur notre système la commande `git` de prête.

## Configuration de git

Nous configurons la commande git :

```bash
git config --global user.name "Prénom Nom"
git config --global user.email "votre@email"
git config --global init.defaultBranch "master"
```

En ayant évidemment remplacé "Prénon Nom" et "votre@email" par les informations de l'un de nous.

Ensuite, on installe `git-gui` :

```bash
sudo apt-get install git-gui
```

Ce dernier se lance en ligne de commande (`git gui`) et affiche l'interface suivante :

![git-gui interface](./pictures/gui-gui.png){ width=350px }

Cette commande-ci permet de créer un dépôt tandis que `gitk` (qui affiche une interface aussi, en apparence plus poussée) doit être lancée dans un dépôt existant.

De notre côté nous n'avons pas eu besoin de définir un proxy, mais nous pourions le faire afin que chaque projet l'utilise :

```bash
git config --global http.proxy <écrire le proxy ici>
```

## Configuration du port 3000

Comme indiqué, nous avons configuré le port 3000 pour que Gitea puisse l'utiliser. Cette configuration nécessite le mode réseau **NAT** (celui par défaut de la machine virtuelle) : 

![ajout du port 3000](./pictures/config-reseau.png)

## Installation de Gitea

Gitea, sous la license MIT, est une solution d'hébergement de code léger gérée par la communauté écrite en Go. Le but de ce logiciel est de proposer un outil simple à mettre en place pour héberger soi-même des repo Git.

Des alternatives connues sont [GitLab](https://about.gitlab.com/) et [GitHub](https://github.com/).

On suit les étapes de la [documentation](https://docs.gitea.io/en-us/install-from-binary/).

Avant toute chose, il nous fallait nous assurer avoir la bonne version de Git (>2.0) :

![version de git](./pictures/git_version.png)

Depuis `wget` on installe le paquet `gitea` comme demandé :

![Package gitea](./pictures/gitea.png)

On nous demande ensuite de créer un utilisateur nommé "git" qui permettra de lancer Gitea :

![Ajout de l'utilisateur git](./pictures/ajout_utilisateur.png)

Avec les dossiers nécessaires :

![création des dossiers](./pictures/directories.png)

![copie du binaire](./pictures/copie_binaire.png)

![export](./pictures/variable.png)

En n'oubliant pas de définir les permissions correctement :

![permissions](./pictures/droits-gitea.png)

750 signifie `rwxr-x---` (tous les droits à l'utilisateur, et droit de lecture et d'exécution/franchissement au groupe, aucun droit aux autres).

640 signifie `rw-r-----` (droits de lecture et d'écriture à l'utilisateur, puis droit de lecture au groupe).

Nous poursuivons en créant le [service](https://docs.gitea.io/en-us/linux-service/) pour le lancement automatique.

![Activer Gitea](./pictures/enable_gitea.png)

L'interface locale :

![gitea fini](./pictures/gitea-fini.png)